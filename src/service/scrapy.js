class Scrapy {
  constructor(){
    let self = this
    this._status = 'login'    
    this._browser = document.getElementById('browserscrapy'),
    this._usuario = '';
    this._senha = '';
    this._url='http://notaparana.pr.gov.br';
    this._data = [];
    this._filaNotas = [];
    this._finish = false;
    this._ultimaRequisicao = new Date()
    setInterval(function () {
      self.verificaSessao()
    }.bind(self), 60000)
    this._browser.addEventListener('console-message', (e) => {
      this.extractReturn(e.message)
    });    
    this._browser.addEventListener('dom-ready', (e) => {
      this._ultimaRequisicao = new Date()
      this.verificaLogin()
    });        
    this._browser.addEventListener("update-target-url", () => {
      var self = this
      this._ultimaRequisicao = new Date()
      setTimeout(function() {self.extractScript()}.bind(self),1000)      
    });      
  }
  carregarNotas () {
    let self = this
    this._notas = firebase.database().ref('notas/'+document.querySelector('#entidade').value+'/');
    this._notas.orderByChild('lancada').equalTo(false).limitToFirst(50).on('child_added', function(data) {
      let nota = data.val()      
      if ('lancada' in nota) {
        if (!nota.lancada) {
          self.lancarNota(data)
        }
      } else {        
        self.lancarNota(data)
      }
    }.bind(self));
  }
  verificaSessao () {
    var agora = new Date()    
    if (agora % this._ultimaRequisicao / 1000 / 60 > 5) {
      console.log('renovando sessao')
      this._browser.src = 'https://notaparana.pr.gov.br/nfprweb/DoacaoDocumentoFiscalCadastrar'
    } else {
      console.log('sessao em dia')
    }
  }
  lancarNota(nota){
    let notaObjeto = nota.val()
    if ('entidade' in notaObjeto){
      this._filaNotas.push(nota)
      this.iniciarLancador()
    } else {
      nota.ref.update({
        "lancada": true,
        "status": "erro",
        "mensagem": "Sem Atributo entidade CNPJ"
      })
    }
  }
  iniciarLancador(){
    if (this._status === 'login') {
      console.log('faz o login')
    } else {
      if (this._filaNotas.length > 0){
        if (this._status === 'logado'){
          this._status = 'lancando'          
          this.extractScript()
        }
      }
    }
  }
  calcScriptPost(nota){
    let script =  `
      var nota = JSON.eval(${JSON.stringify(nota)})
      console.log(nota.val())
    `
    this._browser.executeJavaScript(script);
  }
  loadUrl(){ 
    this._browser.src = this._url 
  }
  logar(){
    let script =  `        
      document.querySelector('#attribute').value = '${this._usuario}'
      document.querySelector('#password').value = '${this._senha}'
      document.querySelector('input[type="submit"]').click()
    `
    this._browser.executeJavaScript(script);
  }
  //var nota = JSON.parse('{"chaveNota":"41170979846275000109650010011271421041609173","date":"09/10/2017","deviceId":"4cfcc581809abc0","entidade":68644723000167,"plataforma":"Android","uid":"0MzPbzaLjLTiskYfjLQZCWP8UFG3","vICMS":"15.49","valor":"317.40"}')
  extractScript(){
    let script = ''
    if (this._status === 'lancando') {
      this._status = 'aguardando retorno'
      let nota = this._filaNotas[0].toJSON()
      script =  `        
        var nota = JSON.parse('${JSON.stringify(nota)}')
        document.querySelector("#cnpjEntidade").value = nota.entidade
        document.querySelector('#chaveAcesso').value = nota.chNFe
        document.querySelector('#btnDoarDocumento').click()
      `
      this._browser.executeJavaScript(script);
    } else if (this._status === 'aguardando retorno'){      
      let nota = this._filaNotas[0].toJSON()
      script =  `        
        var nota = JSON.parse('${JSON.stringify(nota)}')        
        nota.mensagem = document.querySelector('#message') == null ? document.querySelector('#mensagem').innerHTML.replace('"','').replace('"','').split('</i>')[1] : document.querySelector('#message').innerHTML.replace('"','').replace('"','').split('</i>')[1]
        console.log('$data:' + JSON.stringify(nota))
      `
      this._browser.executeJavaScript(script);
    }
    else {      
      this.verificaLogin ()
    }
    
  }  
  verificaLogin () {
    if (this._status === 'login') {
      var script =  `
              var retorno = {
                logado: document.querySelector('#password') === null
              }
              console.log('$data:' + JSON.stringify(retorno))
        `
      this._browser.executeJavaScript(script);
    }
  }
  extractReturn(mensagem){
    if (mensagem.indexOf('$data:') > -1) {
      let retorno = JSON.parse(mensagem.substring(6))
      if (retorno.logado) {
        if (this._status === 'login') {
          this._status = 'logado'
          console.log('iniciando lancador apos login')
          this._browser.src = 'https://notaparana.pr.gov.br/nfprweb/DoacaoDocumentoFiscalCadastrar'
          var self = this
          this.carregarNotas()
          setTimeout(function () {
            self.iniciarLancador()
          }.bind(self),2000)
        } 
      } else if (this._status === 'aguardando retorno') {
        let nota = this._filaNotas[0]
        nota.ref.update({
          "lancada": true,
          "status": "sucesso",
          "mensagem": retorno.mensagem
        })
        this._status = 'logado'
        this._filaNotas.shift()
        var self = this
        self.iniciarLancador()
      }
    } else {
      console.log('log navegador : ' + mensagem)
    }
  }

  // acessores
  get data () {return this._data}
  set data (v) {this._data = v}
  get browser () {return this._browser}
  set browser (v) {this._browser = v}
  get url () {return this._url}
  set url (v) {this._url = v}
  get usuario () {return this._usuario}
  set usuario (v) {this._usuario = v}
  get senha () {return this._senha}
  set senha (v) {this._senha = v}

}

